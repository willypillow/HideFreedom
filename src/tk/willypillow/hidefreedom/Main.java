package tk.willypillow.hidefreedom;

import de.robv.android.xposed.*;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

import android.content.pm.ApplicationInfo;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public class Main implements IXposedHookLoadPackage
{
	public void handleLoadPackage(final LoadPackageParam lpparam) throws Throwable
	{
		if (!lpparam.packageName.equals("com.anywhere.gemAuto")&&!lpparam.packageName.equals("com.anywhere.gemhelper"))
		{
			return;
		}
		
		XposedBridge.log("Got " + lpparam.packageName);
		
		XposedHelpers.findAndHookMethod("android.app.ContextImpl", lpparam.classLoader, "getPackageManager", new XC_MethodHook()
		{
			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable
			{
				Object instance = param.getResult();
				XC_MethodHook newMethod = new XC_MethodHook()
				{
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{
						@SuppressWarnings("unchecked")
						List<ApplicationInfo> list = (List<ApplicationInfo>) param.getResult();
						for(Iterator<ApplicationInfo> iterator = list.iterator();iterator.hasNext();)
						{
							ApplicationInfo ai = iterator.next();
							if(ai.packageName.contains("freedom"))
							{
								iterator.remove();
							}
						}
						XposedBridge.log("Sucessfully hooked!");
						param.setResult(list);
						return;
					}
				};
				Class<? extends Object> resultClass = instance.getClass();
				for(Method method : resultClass.getDeclaredMethods())
				{
					if(method.getName().equals("getInstalledApplications") && (Modifier.isPublic(method.getModifiers())))
					{
						XposedBridge.hookMethod(method , newMethod);
					}
				}
			}
		});
		return;
	}
}